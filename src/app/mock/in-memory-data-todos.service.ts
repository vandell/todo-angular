import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Todo } from '../models/todo';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const todos: Todo[] = [
      {
        id: 1,
        title: 'morning jogging',
        description: 'must be in great moon',
        state: false
      },
      {
        id: 2,
        title: 'do kitchen',
        description: 'dont forget the salt',
        state: false
      },
      {
        id: 3,
        title: 'sleep at 20pm',
        description: 'Tomorrow gonna be a good day',
        state: false
      }
    ];
    return { todos };
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(todos: Todo[]): number {
    return todos.length > 0 ? Math.max(...todos.map((todo) => todo.id)) + 1 : 11;
  }
}
