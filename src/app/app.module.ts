import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { RootReducers, metaReducers } from './reducers';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryDataService } from './mock/in-memory-data-todos.service';
import { EffectsModule } from '@ngrx/effects';
import { RootEffects } from './effects';
import { ReactiveFormsModule } from '@angular/forms';

import { appRoutes } from './routes';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppComponent } from './app.component';
import { TodoListComponent, TodoListAddDialogComponent } from './components/todo/todo-list/todo-list.component';
import { TodoDetailComponent } from './components/todo/todo-detail/todo-detail.component';
import { TodoItemComponent } from './components/todo/todo-item/todo-item.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomeComponent } from './components/home/home.component';
import { MaterialModule } from './modules/material.module';

@NgModule({
  entryComponents: [TodoListAddDialogComponent],
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoListAddDialogComponent,
    TodoItemComponent,
    TodoDetailComponent,
    PageNotFoundComponent,
    HomeComponent
  ],
  imports: [
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(RootReducers, { metaReducers }),
    EffectsModule.forRoot(RootEffects),
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Restrict extension to log-only mode
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
