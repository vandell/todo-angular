import {
  Component,
  OnInit,
  AfterViewInit,
  AfterContentChecked,
  ChangeDetectionStrategy
} from '@angular/core';
import { RootState } from './reducers';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TodosFetch } from './actions/todo/todo.actions';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  isFetching: boolean;
  title: string;

  constructor(private store: Store<RootState>) {
    this.store.pipe(select(state => state.app)).subscribe(appState => {
      this.isFetching = appState.isFetching;
      this.title = appState.title;
    });
  }

  reFetchTodos = () => this.store.dispatch(new TodosFetch());
}
