export class Todo {
  id: number;
  title: string;
  state: boolean;
  description?: string;
}
