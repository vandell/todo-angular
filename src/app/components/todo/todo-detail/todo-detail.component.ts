import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { RootState } from '../../../reducers';
import { AppChangeTitle } from '../../../actions/app/app.actions';
import { Todo } from '../../../models/todo';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TodosFetch } from '../../../actions/todo/todo.actions';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.sass']
})
export class TodoDetailComponent implements OnInit {
  todos$: Observable<Todo[]>;
  todo: Todo;

  constructor(private store: Store<RootState>, private route: ActivatedRoute) {
    this.todos$ = store.pipe(select(state => state.todos.todos));
  }

  ngOnInit() {
    const todoRouteId = this.route.snapshot.paramMap.get('id');
    this.todos$.subscribe(todos => {
      if (!todos.length) {
        setTimeout(() => this.store.dispatch(new TodosFetch()), 0);
      }
      this.todo = todos.find(todo => todo.id === Number(todoRouteId));
    });
  }
}
