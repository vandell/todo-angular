import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.sass']
})
export class TodoItemComponent implements OnInit {
  @Input() id: number;
  @Input() state: boolean;
  @Input() title: string;
  @Input() description: string;
  @Output() changeTodoState = new EventEmitter();
  todoClasses = {};

  ngOnInit(): void {
    this.setClasses();
  }
  setClasses = () => (this.todoClasses = { 'todo-done': this.state });
}
