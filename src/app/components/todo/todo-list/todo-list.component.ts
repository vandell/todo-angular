import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Todo } from '../../../models/todo';
import { Observable } from 'rxjs';
import { TodosFetch, TodosChangeState, TodosCreate } from '../../../actions/todo/todo.actions';
import { TodoState } from '../../../reducers/todo/todo.reducer';
import { RootState } from '../../../reducers';
import { AppChangeTitle } from '../../../actions/app/app.actions';
import { MatDialog } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo-add-dialog',
  templateUrl: 'todo-list-add-dialog.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListAddDialogComponent {
  todoForm = this.fb.group({
    title: ['', Validators.required],
    description: ['']
  });

  createTodo = () => {
    this.store.dispatch(new TodosCreate({ todo: this.todoForm.value }));
    this.todoForm.reset();
  }

  constructor(private fb: FormBuilder, private store: Store<RootState>) {}
}

@Component({
  selector: 'app-todo',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {
  todos: Todo[];
  fetching: boolean;
  todosToDo: Todo[];
  todosDone: Todo[];

  constructor(private store: Store<RootState>, public dialog: MatDialog) {
    store.pipe(select(state => state.todos)).subscribe(todoState => {
      this.todos = todoState.todos;
      this.fetching = todoState.fetchingTodosPending;
      this.todosToDo = this.todos.filter(todo => todo.state === false);
      this.todosDone = this.todos.filter(todo => todo.state === true);
    });
  }

  ngOnInit(): void {
    if (!this.todos.length) {
      setTimeout(() => this.store.dispatch(new TodosFetch()));
    }
  }

  onChangeTodoState = (id: number) => this.store.dispatch(new TodosChangeState({ id }));

  openDialog() {
    this.dialog.open(TodoListAddDialogComponent);
  }
}
