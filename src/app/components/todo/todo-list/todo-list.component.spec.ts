import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { TodoListComponent, TodoListAddDialogComponent } from './todo-list.component';
import { Store, StoreModule } from '@ngrx/store';
import {
  MatListModule,
  MatDividerModule,
  MatButtonModule,
  MatDialogModule,
  MatDialog
} from '@angular/material';
import { RootState, RootReducers, metaReducers } from '../../../reducers';
import { DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TodosFetch, TodosFetchSuccess } from '../../../actions/todo/todo.actions';
import { By } from '@angular/platform-browser';
import { Todo } from '../../../models/todo';
import { TodoService } from '../../../services/todo.service';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { RootEffects } from '../../../effects';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from '../../../mock/in-memory-data-todos.service';

const fakeTodosDone = [
  {
    id: 3,
    description: 'this todo is done',
    title: 'todo 2',
    state: true
  }
];
const fakeTodosToDo = [
  {
    id: 1,
    description: 'do this todo',
    title: 'todo 1',
    state: false
  },
  {
    id: 2,
    description: 'do this todo 2',
    title: 'todo 2',
    state: false
  }
];
const fakeTodos: Todo[] = [...fakeTodosDone, ...fakeTodosToDo];

describe('TodoComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;
  let store: Store<RootState>;
  let el: DebugElement;
  let todoService: TodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDividerModule,
        MatListModule,
        MatButtonModule,
        MatDialogModule,
        HttpClientModule,
        EffectsModule.forRoot(RootEffects),
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false }),
        StoreModule.forRoot(RootReducers, { metaReducers })
      ],
      declarations: [TodoListComponent],
      providers: [TodoService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement;

    store = TestBed.get(Store);
    todoService = TestBed.get(TodoService);

    fixture.detectChanges();
  });

  describe('Unit Test', () => {
    beforeEach(() => {
      spyOn(store, 'dispatch').and.callThrough();
    });
    it('should create the todo-list component', () => {
      expect(component).toBeTruthy();
    });

    it('should show the loading todos text on fetching', () => {
      store.dispatch(new TodosFetch());
      fixture.detectChanges();
      const fetchingTextElement = el.query(By.css('#loading'));
      const fetchingText = fetchingTextElement.nativeElement.innerText;
      expect(fetchingTextElement).toBeTruthy();
      expect(fetchingText).toBe('Loading Todos ...');
    });

    it('should hide the loading todos text when fetching ending', () => {
      store.dispatch(new TodosFetchSuccess({ todos: fakeTodos }));
      fixture.detectChanges();
      const fetchingTextElement = el.query(By.css('#loading'));
      expect(fetchingTextElement).toBeFalsy();
    });

    it('should load todos on start if no one', fakeAsync(() => {
      const todoFetchAction = new TodosFetch();
      fixture.whenStable().then(() => {
        expect(store.dispatch).toHaveBeenCalledWith(todoFetchAction);
      });
    }));

    it('should not shown list until is fetching', () => {
      store.dispatch(new TodosFetch());
      fixture.detectChanges();
      const componentListTodoDone = el.query(By.css('.todo-done'));
      const componentListTodoToDo = el.query(By.css('.todo-todo'));
      expect(componentListTodoDone).toBeFalsy();
      expect(componentListTodoToDo).toBeFalsy();
    });

    it('should show list if fetching ending and display todos on the right category', fakeAsync(() => {
      store.dispatch(new TodosFetchSuccess({ todos: fakeTodos }));
      fixture.detectChanges();
      const componentListTodoDone = el.query(By.css('.todo-done'));
      const componentListTodoToDo = el.query(By.css('.todo-todo'));
      const todoItemDone = el.queryAll(By.css('.todo-done app-todo-item'));
      const todoItemToDo = el.queryAll(By.css('.todo-todo app-todo-item'));
      expect(componentListTodoDone).toBeTruthy();
      expect(componentListTodoToDo).toBeTruthy();
      expect(todoItemDone.length).toBe(1);
      expect(todoItemToDo.length).toBe(2);
      expect(component.todos).toEqual(fakeTodos);
      expect(component.todosDone).toEqual(fakeTodosDone);
      expect(component.todosToDo).toEqual(fakeTodosToDo);
    }));

    it('should open form todo dialog on click', () => {
      spyOn<TodoListComponent>(component, 'openDialog').and.callThrough();
      spyOn<MatDialog>(component.dialog, 'open');
      const button = el.query(By.css('.button-form-todo-dialog'));
      button.triggerEventHandler('click', null);
      expect(component.openDialog).toHaveBeenCalled();
      expect(component.dialog.open).toHaveBeenCalledWith(TodoListAddDialogComponent);
    });
  });

  describe('Integration Test', () => {
    // beforeEach(() => {
    //   spyOn(store, 'dispatch').and.callThrough();
    // });
    // it('should load todos if no one', () => {
    //   const todoFetchAction = new TodosFetch();
    //   fixture.whenStable().then(() => {
    //     fixture.detectChanges();
    //     expect(store.dispatch).toHaveBeenCalledWith(todoFetchAction);
    //   });
    // });
    // it('should load todos if no one 2', fakeAsync(() => {
    //   const todoFetchAction = new TodosFetch();
    //   component.ngOnInit();
    //   tick();
    //   fixture.detectChanges();
    //   expect(store.dispatch).toHaveBeenCalledWith(todoFetchAction);
    // }));
    // it('should not shown list until is fetching', () => {
    //   component.ngOnInit();
    //   const componentListTodoDone = el.query(By.css('.todo-done'));
    //   const componentListTodoToDo = el.query(By.css('.todo-todo'));
    //   expect(componentListTodoDone).toBeFalsy();
    //   expect(componentListTodoToDo).toBeFalsy();
    // });
    // it('should show list if fetching ending', () => {
    //   spyOn<TodoService>(todoService, 'getTodos').and.returnValue(fakeTodos);
    //   component.ngOnInit();
    //   tick(40000);
    //   fixture.detectChanges();
    //   console.log(component.todos, component.fetching);
    //   expect(todoService.getTodos).toHaveBeenCalled();
    //   console.log(component.todos);
    //   console.log(component.fetching);
    //   const componentListTodoDone = el.query(By.css('.todo-done'));
    //   const componentListTodoToDo = el.query(By.css('.todo-todo'));
    //   expect(componentListTodoDone).toBeTruthy();
    //   expect(componentListTodoToDo).toBeTruthy();
    // });
  });
});
