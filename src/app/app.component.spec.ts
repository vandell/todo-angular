import { TestBed, async, ComponentFixture, tick, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {
  MatProgressBarModule,
  MatIconModule,
  MatToolbarModule,
  MatListModule,
  MatButtonModule
} from '@angular/material';
// import { TodoComponent } from './components/todo/todo.component';
import { RootState, RootReducers, metaReducers } from './reducers';
import { StoreModule, Store, select } from '@ngrx/store';
import { TodosFetch } from './actions/todo/todo.actions';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { AppIsFetching, AppChangeTitle } from './actions/app/app.actions';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let store: Store<RootState>;
  let el: DebugElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        // RouterTestingModule,
        MatProgressBarModule,
        MatIconModule,
        MatToolbarModule,
        MatProgressBarModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        StoreModule.forRoot(RootReducers, { metaReducers })
      ],
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: []
    }).compileComponents();

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement;
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should show Loading Bar on fetching', () => {
    store.dispatch(new AppIsFetching({ fetching: true }));
    fixture.detectChanges();
    expect(component.isFetching).toBeTruthy();
    expect(el.query(By.css('mat-progress-bar'))).toBeTruthy();
  });

  it('should disable the "refetchTodo" button on loading', () => {
    store.dispatch(new AppIsFetching({ fetching: true }));
    fixture.detectChanges();
    expect(el.query(By.css('.fetch-todo')).nativeElement.disabled).toBeTruthy();
  });

  it('should load todos on button click "Refetchtodo"', () => {
    const action = new TodosFetch();
    const button = el.query(By.css('.fetch-todo'));
    button.triggerEventHandler('click', null);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should change app title on dispatching a new title', () => {
    const title = 'Test Title';
    store.dispatch(new AppChangeTitle({ title }));
    fixture.detectChanges();
    expect(component.title).toBe(title);
  });
});
