import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { catchError, map, mergeMap, delay, tap, concatMap, withLatestFrom } from 'rxjs/operators';
import {
  TodoActionTypes,
  TodosChangeState,
  TodosChangeStateSuccess,
  TodosChangeStateFailed,
  TodosCreate,
  TodosCreateSuccess,
  TodosCreateFailed
} from '../../actions/todo/todo.actions';
import { AppIsFetching } from '../../actions/app/app.actions';
import { RootState } from '../../reducers';
import { TodoService } from '../../services/todo.service';

@Injectable()
export class TodoEffects {
  constructor(
    private todoService: TodoService,
    private actions$: Actions,
    private store: Store<RootState>
  ) {}

  @Effect()
  public getTodos$: Observable<Action> = this.actions$.pipe(
    ofType(TodoActionTypes.TODOS_FETCH),
    map(() => this.store.dispatch(new AppIsFetching({ fetching: true }))),
    mergeMap(this.todoService.getTodos)
  );

  @Effect()
  public changeTodoState$: Observable<Action> = this.actions$.pipe(
    ofType<TodosChangeState>(TodoActionTypes.TODOS_CHANGE_STATE),
    tap(() => this.store.dispatch(new AppIsFetching({ fetching: true }))),
    mergeMap(action => this.todoService.changeTodoState(action.payload.id))
  );

  @Effect()
  public createTodo$: Observable<Action> = this.actions$.pipe(
    ofType<TodosCreate>(TodoActionTypes.TODOS_CREATE),
    tap(() => this.store.dispatch(new AppIsFetching({ fetching: true }))),
    withLatestFrom(this.store),
    mergeMap(([action, storeState]) => {
      const newTodoId = storeState.todos.todos.length + 1;
      const newTodo = { ...action.payload.todo, id: newTodoId };

      return this.todoService.createTodo(newTodo);
    })
  );
}
