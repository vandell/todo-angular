import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, concatMap, catchError } from 'rxjs/operators';
import {
  TodosFetchSuccess,
  TodosFetchFailed,
  TodosChangeStateSuccess,
  TodosChangeStateFailed,
  TodosCreateSuccess,
  TodosCreateFailed
} from '../actions/todo/todo.actions';
import { AppIsFetching } from '../actions/app/app.actions';
import { of } from 'rxjs';
import { Todo } from '../models/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  urlApi = '/api/todos';
  constructor(private http: HttpClient) {}

  getTodos = () =>
    this.http.get(this.urlApi).pipe(
      delay(1000),
      concatMap((data: Todo[]) => [
        new TodosFetchSuccess({ todos: data }),
        new AppIsFetching({ fetching: false })
      ]),
      catchError(() => of(new TodosFetchFailed()))
    )

  changeTodoState = (id: number) =>
    this.http.get(`${this.urlApi}/${id}/change-state`).pipe(
      concatMap(() => [
        new TodosChangeStateSuccess({ id }),
        new AppIsFetching({ fetching: false })
      ]),
      catchError(() => of(new TodosChangeStateFailed()))
    )

  createTodo = (todo: Todo) => {
    const newTodoToReducerFake = { ...todo, state: false, id: todo.id };
    return this.http.post(this.urlApi, { todo }).pipe(
      concatMap(() => [
        new TodosCreateSuccess({ todo: newTodoToReducerFake }),
        new AppIsFetching({ fetching: false })
      ]),
      catchError(() => of(new TodosCreateFailed()))
    );
  }
}
