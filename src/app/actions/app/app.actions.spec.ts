import { AppIsFetching, AppActionTypes, AppChangeTitle } from './app.actions';

describe('App Action', () => {
  describe('AppIsFetching', () => {
    it('should create an action', () => {
      const payload: AppIsFetching['payload'] = { fetching: true };
      const action = new AppIsFetching(payload);
      expect({ ...action }).toEqual({
        type: AppActionTypes.isFetching,
        payload
      });
    });
  });

  describe('AppChangeTitle', () => {
    it('should create an action', () => {
      const payload: AppChangeTitle['payload'] = { title: 'MyTitle' };
      const action = new AppChangeTitle(payload);
      expect({ ...action }).toEqual({
        type: AppActionTypes.changeTitle,
        payload
      });
    });
  });
});
