import { Action } from '@ngrx/store';

export enum AppActionTypes {
  isFetching = '[App] is Fetching',
  changeTitle = '[App] change Title'
}

export class AppIsFetching implements Action {
  readonly type = AppActionTypes.isFetching;
  constructor(public payload: { fetching: boolean }) {}
}

export class AppChangeTitle implements Action {
  readonly type = AppActionTypes.changeTitle;
  constructor(public payload: { title: string }) {}
}

export type AppActions = AppIsFetching | AppChangeTitle;
