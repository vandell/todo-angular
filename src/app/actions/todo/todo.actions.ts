import { Action } from '@ngrx/store';
import { Todo } from '../../models/todo';

export enum TodoActionTypes {
  TODOS_FETCH = '[Todo] fetch Todos',
  TODOS_FETCH_SUCCESS = '[Todo] fetch Todos success',
  TODOS_FETCH_FAILED = '[Todo] fetch Todos failed',
  TODOS_CHANGE_STATE = '[Todo] change a todo state',
  TODOS_CHANGE_STATE_SUCCESS = '[Todo] change a todo state success',
  TODOS_CHANGE_STATE_FAILED = '[Todo] change a todo state failed',
  TODOS_CREATE = '[Todo] create a todo ',
  TODOS_CREATE_SUCCESS = '[Todo] create a todo success',
  TODOS_CREATE_FAILED = '[Todo] create a todo failed'
}

export class TodosFetch implements Action {
  readonly type = TodoActionTypes.TODOS_FETCH;
}

export class TodosFetchSuccess implements Action {
  readonly type = TodoActionTypes.TODOS_FETCH_SUCCESS;
  constructor(public payload: { todos: Todo[] }) {}
}

export class TodosFetchFailed implements Action {
  readonly type = TodoActionTypes.TODOS_FETCH_FAILED;
}

export class TodosChangeState implements Action {
  readonly type = TodoActionTypes.TODOS_CHANGE_STATE;
  constructor(public payload: { id: number }) {}
}

export class TodosChangeStateSuccess implements Action {
  readonly type = TodoActionTypes.TODOS_CHANGE_STATE_SUCCESS;
  constructor(public payload: { id: number }) {}
}

export class TodosChangeStateFailed implements Action {
  readonly type = TodoActionTypes.TODOS_CHANGE_STATE_FAILED;
}

export class TodosCreate implements Action {
  readonly type = TodoActionTypes.TODOS_CREATE;
  constructor(public payload: { todo: Todo }) {}
}

export class TodosCreateSuccess implements Action {
  readonly type = TodoActionTypes.TODOS_CREATE_SUCCESS;
  constructor(public payload: { todo: Todo }) {}
}

export class TodosCreateFailed implements Action {
  readonly type = TodoActionTypes.TODOS_CREATE_FAILED;
}

export type TodoActions =
  | TodosFetch
  | TodosFetchSuccess
  | TodosFetchFailed
  | TodosChangeState
  | TodosChangeStateSuccess
  | TodosChangeStateFailed
  | TodosCreate
  | TodosCreateSuccess
  | TodosCreateFailed;
