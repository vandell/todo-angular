import {
  TodosFetch,
  TodoActionTypes,
  TodosFetchSuccess,
  TodosFetchFailed,
  TodosChangeState,
  TodosChangeStateSuccess,
  TodosChangeStateFailed,
  TodosCreate,
  TodosCreateSuccess,
  TodosCreateFailed
} from './todo.actions';

const fakeTodos = [
  {
    id: 1,
    description: 'do this todo',
    title: 'todo 1',
    state: false
  },
  {
    id: 2,
    description: 'do this todo 2',
    title: 'todo 2',
    state: false
  },
  {
    id: 3,
    description: 'this todo is done',
    title: 'todo 2',
    state: true
  }
];

describe('Todo Action', () => {
  describe('TodosFetch', () => {
    it('should create an action', () => {
      const action = new TodosFetch();
      expect({ ...action }).toEqual({ type: TodoActionTypes.TODOS_FETCH });
    });
  });

  describe('TodosFetchSuccess', () => {
    it('should create an action', () => {
      const payload: TodosFetchSuccess['payload'] = { todos: fakeTodos };
      const action = new TodosFetchSuccess(payload);
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_FETCH_SUCCESS,
        payload
      });
    });
  });

  describe('TodosFetchFailed', () => {
    it('should create an action', () => {
      const action = new TodosFetchFailed();
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_FETCH_FAILED
      });
    });
  });

  describe('TodosChangeState', () => {
    it('should create an action', () => {
      const payload: TodosChangeState['payload'] = { id: 4 };
      const action = new TodosChangeState(payload);
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CHANGE_STATE,
        payload
      });
    });
  });

  describe('TodosChangeStateSuccess', () => {
    it('should create an action', () => {
      const payload: TodosChangeStateSuccess['payload'] = { id: 4 };
      const action = new TodosChangeStateSuccess(payload);

      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CHANGE_STATE_SUCCESS,
        payload
      });
    });
  });

  describe('TodosChangeStateFailed', () => {
    it('should create an action', () => {
      const action = new TodosChangeStateFailed();
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CHANGE_STATE_FAILED
      });
    });
  });

  describe('TodosCreate', () => {
    it('should create an action', () => {
      const payload: TodosCreate['payload'] = { todo: fakeTodos[0] };
      const action = new TodosCreate(payload);
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CREATE,
        payload
      });
    });
  });

  describe('TodosCreateSuccess', () => {
    it('should create an action', () => {
      const payload: TodosCreateSuccess['payload'] = { todo: fakeTodos[0] };
      const action = new TodosCreateSuccess(payload);
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CREATE_SUCCESS,
        payload
      });
    });
  });

  describe('TodoCreateFailed', () => {
    it('should create an action', () => {
      const action = new TodosCreateFailed();
      expect({ ...action }).toEqual({
        type: TodoActionTypes.TODOS_CREATE_FAILED
      });
    });
  });
});
