import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
  StoreModule
} from '@ngrx/store';

import { environment } from '../../environments/environment';
import * as fromTodo from './todo/todo.reducer';
import * as fromApp from './app/app.reducer';

export interface RootState {
  todos: fromTodo.TodoState;
  app: fromApp.AppState;
}

export const RootReducers: ActionReducerMap<RootState> = {
  todos: fromTodo.reducer,
  app: fromApp.reducer
};

export const metaReducers: MetaReducer<RootState>[] = !environment.production ? [] : [];
