import { TodoActions, TodoActionTypes } from '../../actions/todo/todo.actions';
import { Todo } from '../../models/todo';

export interface TodoState {
  todos: Todo[];
  fetchingTodosPending: boolean;
  changeStateTodoPending: boolean;
  createTodoPending: boolean;
}

export const initialState: TodoState = {
  todos: [],
  fetchingTodosPending: false,
  changeStateTodoPending: false,
  createTodoPending: false
};

export function reducer(state = initialState, action: TodoActions): TodoState {
  switch (action.type) {
    case TodoActionTypes.TODOS_FETCH: {
      return { ...state, fetchingTodosPending: true };
    }
    case TodoActionTypes.TODOS_FETCH_SUCCESS: {
      return { ...state, fetchingTodosPending: false, todos: action.payload.todos };
    }
    case TodoActionTypes.TODOS_FETCH_FAILED: {
      return { ...state, fetchingTodosPending: false };
    }
    case TodoActionTypes.TODOS_CHANGE_STATE: {
      return { ...state, changeStateTodoPending: true };
    }
    case TodoActionTypes.TODOS_CHANGE_STATE_SUCCESS: {
      const id = action.payload.id;
      const todos = state.todos.map(todo => (todo.id === id ? { ...todo, state: true } : todo));
      return { ...state, todos, changeStateTodoPending: false };
    }
    case TodoActionTypes.TODOS_CHANGE_STATE_FAILED: {
      return { ...state, changeStateTodoPending: false };
    }
    case TodoActionTypes.TODOS_CREATE: {
      return { ...state, createTodoPending: true };
    }
    case TodoActionTypes.TODOS_CREATE_SUCCESS: {
      const newTodo = action.payload.todo;
      return { ...state, todos: [newTodo, ...state.todos], createTodoPending: false };
    }
    case TodoActionTypes.TODOS_CREATE_FAILED: {
      return { ...state, createTodoPending: false };
    }
    default:
      return state;
  }
}
