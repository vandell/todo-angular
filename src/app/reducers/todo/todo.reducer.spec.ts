import { reducer, initialState, TodoState } from './todo.reducer';
import {
  TodosFetch,
  TodosFetchSuccess,
  TodosFetchFailed,
  TodosChangeState,
  TodosChangeStateSuccess,
  TodosChangeStateFailed,
  TodosCreate,
  TodosCreateSuccess,
  TodosCreateFailed
} from '../../actions/todo/todo.actions';
import { Todo } from '../../models/todo';

const fakeTodos = [
  {
    id: 1,
    description: 'do this todo',
    title: 'todo 1',
    state: false
  },
  {
    id: 2,
    description: 'do this todo 2',
    title: 'todo 2',
    state: false
  },
  {
    id: 3,
    description: 'this todo is done',
    title: 'todo 2',
    state: true
  }
];

describe('Todo Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });
  describe('On Todo Fetching', () => {
    it('should set fetching to true', () => {
      const action = new TodosFetch();
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, fetchingTodosPending: true };
      expect(result).toEqual(expectResult);
    });

    it('should turn off pending and give new Todos', () => {
      const action = new TodosFetchSuccess({ todos: fakeTodos });
      const result = reducer(initialState, action);
      const expectResult: TodoState = {
        ...initialState,
        fetchingTodosPending: false,
        todos: fakeTodos
      };
      expect(result).toEqual(expectResult);
    });

    it('should turn off fetching on failed', () => {
      const action = new TodosFetchFailed();
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, fetchingTodosPending: false };
      expect(result).toEqual(expectResult);
    });
  });
  describe('On Todo Changing state', () => {
    it('should turn on pending on change state todo', () => {
      const action = new TodosChangeState({ id: 1 });
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, changeStateTodoPending: true };
      expect(result).toEqual(expectResult);
    });

    it('should change state from one todo', () => {
      const action = new TodosChangeStateSuccess({ id: 1 });
      const initialStateWithTodo = { ...initialState, todos: fakeTodos };
      const result = reducer(initialStateWithTodo, action);
      const expectedTodosWithNewState: Todo[] = fakeTodos.map(todo =>
        todo.id === 1 ? { ...todo, state: true } : todo
      );
      const expectResult = {
        ...initialStateWithTodo,
        todos: expectedTodosWithNewState
      };
      expect(result).toEqual(expectResult);
    });

    it('should turn off pending on fail', () => {
      const action = new TodosChangeStateFailed();
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, changeStateTodoPending: false };
      expect(result).toEqual(expectResult);
    });
  });
  describe('On Todo creating', () => {
    const newTodo: Todo = { id: 4, title: 'new todo', description: 'my new todo', state: false };
    it('should turn on pending on creating a new todo', () => {
      const action = new TodosCreate({ todo: newTodo });
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, createTodoPending: true };
      expect(result).toEqual(expectResult);
    });
    it('should create a new todo', () => {
      const initialStateWithTodo = { ...initialState, todos: fakeTodos };
      const action = new TodosCreateSuccess({ todo: newTodo });
      const expectResult: TodoState = {
        ...initialStateWithTodo,
        todos: [newTodo, ...initialStateWithTodo.todos],
        createTodoPending: false
      };

      const result = reducer(initialStateWithTodo, action);
      expect(result).toEqual(expectResult);
    });

    it('should turn off pending on fail', () => {
      const action = new TodosCreateFailed();
      const result = reducer(initialState, action);
      const expectResult: TodoState = { ...initialState, createTodoPending: false };
      expect(result).toEqual(expectResult);
    });
  });
});
