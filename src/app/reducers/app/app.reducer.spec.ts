import { reducer, initialState } from './app.reducer';
import { AppIsFetching, AppChangeTitle } from '../../actions/app/app.actions';

describe('App Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });
  describe('on App state change', () => {
    it('should set fetching state from the payload', () => {
      const action = new AppIsFetching({ fetching: true });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ...initialState, isFetching: true });
    });

    it('should change the app title', () => {
      const action = new AppChangeTitle({ title: 'MyTitle' });
      const result = reducer(initialState, action);
      expect(result).toEqual({ ...initialState, title: 'MyTitle' });
    });
  });
});
