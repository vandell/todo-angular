import { Action } from '@ngrx/store';
import { AppActionTypes, AppActions } from '../../actions/app/app.actions';

export interface AppState {
  isFetching: boolean;
  title: string;
}

export const initialState: AppState = {
  isFetching: false,
  title: 'Todo Application'
};

export function reducer(state = initialState, action: AppActions): AppState {
  switch (action.type) {
    case AppActionTypes.isFetching: {
      return { ...state, isFetching: action.payload.fetching };
    }
    case AppActionTypes.changeTitle: {
      return { ...state, title: action.payload.title };
    }
    default:
      return state;
  }
}
